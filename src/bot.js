import IRC from 'irc-framework';
import c from 'irc-colors';

import Config from './config';
import { glitter } from './effects';
import { drinks, snacks } from './food';

const bot = new IRC.Client();
const channel = bot.channel(Config.channel);

let partyIsStarted = false;
let lastSnacker = '';
let lastDrinker = '';

function usage() {
  channel.say(`Iemand een ${c.bold('hapje')} of een ${c.bold('drankje')}?`);
}

bot.connect({
  host: Config.host,
  port: Config.port,
  nick: Config.nick,
  ssl: Config.ssl,
  auto_reconnect: Config.auto_reconnect,
});

bot.on('connected', () => {
  channel.join();
});

bot.on('join', (event) => {
  if (bot.user.nick === event.nick) {
    channel.say(`Hey iedereen! Ik ben uw gastheer vandaag, ${bot.user.nick}!`);
    channel.say(c.bold.rainbow(glitter));
    channel.say(`Het feestje begint om ${c.bold(Config.partyStartHour)}u maar maak het alvast gezellig hier!`);
  }
});

bot.on('join', (event) => {
  if (bot.user.nick !== event.nick) {
    channel.say(c.bold.rainbow(glitter));
    channel.say(c.bold.red(`Yow ${c.underline(event.nick)}! Welkom op het ${c.underline('feest')}!`));
    if (!partyIsStarted) {
      channel.say(c.bold(`Het begint pas echt om ${Config.partyStartHour}u...`));
    }
    channel.say(c.bold.rainbow(glitter));
  }
});

bot.matchMessage(/(^.*ik.*hapje|^.*nog.*hapje|^.*geef.*hapje|^.*ik.*honger)/i, (event) => {
  if (bot.user.nick !== event.nick && partyIsStarted) {
    if (snacks.total > 0) {
      if (event.nick !== lastSnacker) {
        snacks.total -= 1;
        lastSnacker = event.nick;
        const item = snacks.items[Math.floor(Math.random() * snacks.items.length)];
        bot.action(Config.channel, `geeft ${event.nick} ${c.bold(item)}. Smakelijk.`);
      } else {
        event.reply(`${event.nick}: laat de rest ook eerst iets eten.`);
      }
    } else {
      event.reply('De hapjes zijn op :(');
    }
  }
});

bot.matchMessage(/(^.*ik.*drank|^.*nog.*drank|^.*geef.*drank|^.*ik.*dorst)/i, (event) => {
  if (bot.user.nick !== event.nick && partyIsStarted) {
    if (drinks.total > 0) {
      if (event.nick !== lastDrinker) {
        drinks.total -= 1;
        lastDrinker = event.nick;
        const item = drinks.items[Math.floor(Math.random() * drinks.items.length)];
        bot.action(Config.channel, `geeft ${event.nick} ${c.bold(item)}. Santé.`);
      } else {
        event.reply(`${event.nick}: jij hebt nog maar drank gehad...`);
      }
    } else {
      event.reply('De drank is op :(');
    }
  }
});

bot.matchMessage(/(grappig|lol|haha)/i, (event) => {
  if (bot.user.nick !== event.nick && partyIsStarted) {
    event.reply(`${event.nick}: lol`);
  }
});

bot.matchMessage(/(confetti|slinger|glitter)/i, (event) => {
  if (bot.user.nick !== event.nick && partyIsStarted) {
    channel.say(c.bold.rainbow(glitter));
  }
});

bot.matchMessage(/(thx|thanks|bedankt|merci)/i, (event) => {
  if (bot.user.nick !== event.nick && partyIsStarted) {
    event.reply(`${event.nick}: graag gedaan! ;)`);
  }
});

// Check if party is started
setInterval(() => {
  const now = new Date();
  if (now.getHours() >= Config.partyStartHour && now.getHours() < Config.partyStopHour) {
    if (partyIsStarted === false) {
      // Party kickoff
      channel.say(`Hey iedereen! Ik ben uw gastheer vandaag, ${bot.user.nick}!`);
      channel.say(c.bold.rainbow('Het feestje is begonnen! PARTYYYYY!!'));
      setTimeout(() => {
        usage();
      }, 2000);
      partyIsStarted = true;
    }
  }
  // Quit when party is finished.
  if (now.getHours() >= Config.partyStopHour) {
    channel.say('Leuk feestje, tot de volgende keer!');
    bot.action(Config.channel, 'is de piste in...');
    channel.say(c.bold.rainbow(glitter));
    bot.quit('xoxo');
    process.exit();
  }
}, 60000);

process.on('SIGINT', () => {
  bot.quit('exiting');
});
