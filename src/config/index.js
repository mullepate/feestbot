const settings = {
  nick: process.env.IRC_NICK || 'FeestBot',
  host: process.env.IRC_SERVER || 'localhost',
  port: process.env.IRC_PORT || 6667,
  ssl: process.env.IRC_SSL !== undefined,
  channel: process.env.IRC_CHANNEL || '#feestbot',
  auto_reconnect: true,
  partyStartHour: 13,
  partyStopHour: 18,
};

export default settings;
