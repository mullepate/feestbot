const drinks = {
  total: 56,
  items: [
    'een bak bier',
    'een beker ovomaltine',
    'een cola',
    'een fristi',
    'een glaasje champagne',
    'een glas rode wijn',
    'een piña colada',
    'een pintje',
    'een shrimp cocktail',
    'een tonic',
    'een watertje',
  ],
};

const snacks = {
  total: 20,
  items: [
    'een bordje kaas',
    'een bordje mini hotdogs',
    'een schotel met versgebakken zakoeski',
    'een selder stengel',
    'een toastje met zalm',
    'fatty tuna sushi',
    'sashimi met vele wasabi',
    'wat borrelnootjes',
    'wat zoute chips',
  ],
};

export { drinks, snacks };
